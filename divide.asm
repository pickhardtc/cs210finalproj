# Bob Roos
# Demonstration of integer division in MIPS
#
# All work is done in the "lo" and "hi" registers. At the conclusion,
# the quotient is in "lo" and the remainder is in "hi". The
# commands "mfhi" ("move from hi") and "mflo" ("move from lo")
# can be used to move the results into ordinary registers.

	.data
	.align	2
a:	.word	8	# dividend
b:	.word	3	# divisor
q:	.asciiz	"Quotient = "
r:	.asciiz	"\nRemainder = "

	.text
	lw	$s0,a	# dividend
	lw	$s1,b	# divisor
	div	$s0,$s1	# hi = s0/s1, lo = s0%s1

	la	$a0,q	# print quotient label
	li	$v0,4
	syscall

	mflo	$a0	# print quotient
	li	$v0,1
	syscall

	la	$a0,r	# print remainder label
	li	$v0,4
	syscall

	mfhi	$a0	# print remainder
	li	$v0,1
	syscall

	li	$v0,10	# program exit
	syscall
