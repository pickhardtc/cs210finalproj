/**
 * Example of the strsep function. This program reads in a
 * string and prints out all the 'words' (i.e., sequences of
 * nonblank characters). All "words" in the input are
 * assumed to be separated by a single space. (Try inputs
 * with leading or trailing blanks, or with strings of two or
 * more blanks inside.)
 *
 * Sample compile and run (note what happens to the extra space):
 *      gcc strsepexample.c -o strsep
 *      ./strsep
 *      Enter a line of text:
 *      this is a  test
 *      Original string: this is a  test
 *      "this"
 *      "is"
 *      "a"
 *      ""
 *      "test"
 */

#include <stdio.h>
#include <string.h>
/* We set LINELENGTH to 82: 80 characters + newline + \0 */
#define LINELENGTH 82

int main() {
   char line[LINELENGTH];     /* The line input by the user */
   char *linepointer; /* Pointer needed by strsep function */
   char *word;        /* A single word of the input */

   printf("Enter a line of text:\n");
   fgets(line,LINELENGTH,stdin);

   line[strlen(line)-1] = '\0'; /* get rid of newline character */
   printf("Original string: %s\n",line);

   /* Get ready to use strsep: */
   linepointer = line; /* Initially, linepointer points to first char in input */
   while((word = strsep(&linepointer," "))!= NULL) {
      printf("\"%s\"\n",word);
   }
}
