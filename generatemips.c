/**Claire Pickhardt & Nate Gervais
 *Professor Roos, Computer Science 210
 *Final Project
 *Honor Code: The work that we are submitting is the result of our own thinking and efforts.
 *This code is designed to take a user's input of an arithmetic equation and translate it into MIPS code.

 * Using the strsep function to parse simple input and generate MIPS code.
 * Input: an arithmetic expression consisting of alternating
 * integer constants and operators. All numbers and operators are
 * separated by a single space.
 *
 * Sample compile and run:
 *
 */

#include <stdio.h>
#include <string.h>
#define LINELENGTH 82


int main() {
    char line[LINELENGTH];     /* The expression input by the user */
    char *linepointer; /* Pointer needed by strsep function */
    char *word;        /* A single number or operator in the input */
    char op;           /* Hold an operator (+ or -) */
    char LastOperator; /*necessary for finishing the line parsing*/
   printf("Enter an integer expression, but please do not use parentheses, \nand separate numbers and operators with spaces please:\n");
   fgets(line,LINELENGTH,stdin);
   line[strlen(line)-1] = '\0'; /* get rid of newline character */
   printf("Paste the following code into MARS:\n");

   printf("    .text\n");

   /* Get ready to use strsep: */
   linepointer = line; /* Initially, linepointer points to first char in input */

   word = strsep(&linepointer," "); /* Read in first number */
   printf("    li    $s0,%s\n",word);


        while((word = strsep(&linepointer," "))!= NULL) {

            if(strlen(word) == 0) continue; /* User entered multiple blanks */

            if(! isdigit(word[0])) { /* if it's not a digit, then it's an operator */
                op = word[0]; /* save the operator until we read in next number */
            }

        else{


         printf("    li    $s1,%s\n",word);

        /*if(op == '+' || op == '-'){
            LastOperator = op;
        }*/

        if(op == '*'){
            printf("    mult  $s0, $s1\n");
            printf("    mflo  $s0\n"); /*moves what is in hi register into $s0*/
        }/*if multiplication*/

        if(op == '/'){
            printf("    div   $s0, $s1\n"); /*$s0 is the quotient and $s1 is the remainder*/
            printf("    mflo  $s0\n");
        }/*if division*/



            if(op == '+') {
                printf("    add   $s2,$s2,$s3\n");
            }/*if statement addition*/

            if(op == '-'){
                printf("    sub   $s2,$s2,$s3\n");
          /*if statement subtraction*/}
            }/*else*/
        }/*while*/
}/*main method*/
